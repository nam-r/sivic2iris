Le lancement du script se fait via la commande suivante :
	python geocode.py

Il est nécessaire d'utiliser la version 3.6 de python

L'installation de package se fait via le système pip.
Installation :
https://www.tecmint.com/install-pip-in-linux/

Pour installer addok il faut au préalable installer python3-dev :
	sudo apt install python3-dev

Documentation d'addok :
https://addok.readthedocs.io/fr/latest/

Il est possible que l'installation du package Rtree requiert l'installation d'une librairie spécifique.
La commande suivante peut être exécutée en cas d'erreur.
	sudo apt install libspatialindex-dev python-rtree


Le chemin du fichier contenant les adresses doit être défini dans la variable : input_file_name
Le chemin du fichier contenant les données à l'IRIS anonymisées doit être défini dans la variable : output_file_name

Le fichier en input doit être un csv dont le séparateur est un \t (tabulation) et encodé en latin-1 
	(voir exemple transmis : "./data/Export_SIVIC_200701_09h56_HIST_SOM_Déraillement_TGV.csv"
Ce fichier doit a minima contenir les colonnes suivantes :
- Num_et_nom_de_voie
- Code_postal
- Ville
- Numero_SINUS

Concernant le hashing du code SIVIC il est nécessaire de modifier la fonction hash_sivic.

Note : le lancement peut être lent pour l'initialisation de la base Redis.
Dans le cas où le script est exécuté de manière fréquente, il est recommandé de ne pas désactiver la base redis.
Dans ce cas, il faut la lancer en utilisant la commande suivante :
	./redis-server.exe redis.conf (Windows)
	./redis-server redis.conf (Linux)
Il faudra alors commenter les lignes suivantes du script geocode.py :
	# redis_cmd = detect_redis_command()
	# proc = subprocess.Popen([redis_cmd, "redis.conf"])

	# proc.terminate()