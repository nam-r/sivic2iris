import subprocess
import sys

# installation of requirements
subprocess.check_call([sys.executable, "-m", "pip", "install", "-r", "requirements.txt"])

import os
import subprocess
from time import sleep

from addok.core import search
from addok.config import config
from redis import Redis
from redis.exceptions import BusyLoadingError

import pandas as pd
import geopandas as gpd
from os import listdir
from rtree import index
from hashlib import sha256

## parameters to edit
# file containing addresses
input_file_name = './data/Export_SIVIC_200701_09h56_HIST_SOM_Deraillement_TGV.csv'
# file to write IRIS code
output_file_name = './data/output.csv'

#reading and postprocessing the input file
df_address = pd.read_csv(input_file_name,sep="\t",dtype = 'str', encoding='latin-1')
df_address = df_address.fillna("")
df_address["address_complete"] = df_address["Num_et_nom_de_voie"] + " " + df_address["Code_postal"] + " " + df_address["Ville"]

def geocode(df, address_column):
        """
        This function geocodes the adresses contained in a pandas dataframe using a specific API. 
        As BAN API geocoding handles csv, a chunk_size is given to define the size of each batch.

        Parameters
        ----------
        df: pd.dataframe
                The dataframe containing addresses to geocode.
        address_column: str[]
                The name of the columns containing addresses.

        Returns
        -------
        gpd.dataframe
                A geopandas dataframe containing the geolocalized points associated to the addresses given as input
        """

        address_nb = df.shape[0]
        l_df_geocode = list()
        for address in df[address_column]:
                print(address)
                print(search(address))
                s_address = (search(address)[0]).format()
                df_geocode = pd.DataFrame.from_dict(s_address)
                l_df_geocode.append(s_address)
        
        print("\nGeocoding over, postprocessing in progress")
        df_geo = gpd.GeoDataFrame.from_features(l_df_geocode)
        print("Done")
        return(df_geo)

def wait_for_redis():
        r = Redis("127.0.0.1")

        while True:
                try:
                        r.ping()
                        break
                except BusyLoadingError:
                        print("Redis is still initializing...")
                        sleep(2)


def detect_redis_command():
        if os.name == "nt":
                return "./redis-server.exe"
        else:
                return "redis-server"

# geocoding step
try:
        os.environ["ADDOK_CONFIG_MODULE"] = "addok.conf"
        redis_cmd = detect_redis_command()
        proc = subprocess.Popen([redis_cmd, "redis.conf"])
        config.load()
        wait_for_redis()
        df_address_geo = geocode(df_address, address_column = 'address_complete')
        df_address = df_address.merge(df_address_geo, how = 'left', left_index = True, right_index = True)
        
finally:
        proc.terminate()
        pass

## Finding IRIS code

# reading IRIS geometries
print("reading IRIS contours")
path = './data/CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139'
folders_to_import = [path + '/' + x + '/' for x in listdir(path) if not x.endswith('.md5')]

df_dict = {}
for folder in folders_to_import:
        folder = folders_to_import[0]
        df_loc = gpd.read_file(folder)
        df_loc['geometry'] = df_loc['geometry'].to_crs(epsg=4326)
        dict_loc = {folder:df_loc}
        df_dict.update(dict_loc)
        df_iris = pd.concat(list(df_dict.values()))

print("Indexing IRIS geometries")
idx = index.Index()
for n, g in enumerate(df_iris['geometry']):
   idx.insert(n,g.bounds)
   
def find_iris_code(p):
        """
        This function provides the iris code associated to a geopandas Point (SRID : WGS84, 4326).

        Parameters 
        ----------
        p: geopandas.series
                corresponds to the point
        Returns
        -------
        string
                the iris code corresponding to the input point
        """
        possible_intesects = list(idx.intersection((p.x,p.y)))
        df_possible = df_iris.loc[possible_intesects,:]
        df_loc = df_possible[df_possible.geometry.intersects(p)]["CODE_IRIS"]
        return(list(df_loc)[0])
        
print("Match with IRIS code")
df_address['code_iris'] = df_address["geometry"].apply(find_iris_code)

## Hashing SIVIC code
def hash_sivic(code):
        return(sha256(code.encode('utf-8')).hexdigest())
print("Hash SIVIC id")
df_address["id"] = df_address["Numero_SINUS"].apply(hash_sivic)

# Removing specific columns
df_address = df_address[['id','score','type','postcode','city','context','citycode','district','code_iris']]

# Writing the output
df_address.to_csv(output_file_name,index=False)
